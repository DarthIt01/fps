// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterMenuItemWidgetStyle_generated_h
#error "ShooterMenuItemWidgetStyle.generated.h already included, missing '#pragma once' in ShooterMenuItemWidgetStyle.h"
#endif
#define SHOOTERGAME_ShooterMenuItemWidgetStyle_generated_h

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShooterMenuItemStyle_Statics; \
	SHOOTERGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FSlateWidgetStyle Super;


template<> SHOOTERGAME_API UScriptStruct* StaticStruct<struct FShooterMenuItemStyle>();

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterMenuItemWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterMenuItemWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterMenuItemWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterMenuItemWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUShooterMenuItemWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterMenuItemWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterMenuItemWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterMenuItemWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterMenuItemWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterMenuItemWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterMenuItemWidgetStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(UShooterMenuItemWidgetStyle&&); \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(const UShooterMenuItemWidgetStyle&); \
public:


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(UShooterMenuItemWidgetStyle&&); \
	SHOOTERGAME_API UShooterMenuItemWidgetStyle(const UShooterMenuItemWidgetStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterMenuItemWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterMenuItemWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterMenuItemWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_50_PROLOG
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_INCLASS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h_53_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterMenuItemWidgetStyle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterMenuItemWidgetStyle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuItemWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
