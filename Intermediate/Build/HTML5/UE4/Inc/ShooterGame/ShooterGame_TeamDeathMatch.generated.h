// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterGame_TeamDeathMatch_generated_h
#error "ShooterGame_TeamDeathMatch.generated.h already included, missing '#pragma once' in ShooterGame_TeamDeathMatch.h"
#endif
#define SHOOTERGAME_ShooterGame_TeamDeathMatch_generated_h

#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterGame_TeamDeathMatch(); \
	friend struct Z_Construct_UClass_AShooterGame_TeamDeathMatch_Statics; \
public: \
	DECLARE_CLASS(AShooterGame_TeamDeathMatch, AShooterGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGame_TeamDeathMatch)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAShooterGame_TeamDeathMatch(); \
	friend struct Z_Construct_UClass_AShooterGame_TeamDeathMatch_Statics; \
public: \
	DECLARE_CLASS(AShooterGame_TeamDeathMatch, AShooterGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGame_TeamDeathMatch)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGame_TeamDeathMatch(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGame_TeamDeathMatch) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGame_TeamDeathMatch); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGame_TeamDeathMatch); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGame_TeamDeathMatch(AShooterGame_TeamDeathMatch&&); \
	NO_API AShooterGame_TeamDeathMatch(const AShooterGame_TeamDeathMatch&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGame_TeamDeathMatch(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGame_TeamDeathMatch(AShooterGame_TeamDeathMatch&&); \
	NO_API AShooterGame_TeamDeathMatch(const AShooterGame_TeamDeathMatch&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGame_TeamDeathMatch); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGame_TeamDeathMatch); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGame_TeamDeathMatch)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_10_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterGame_TeamDeathMatch."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterGame_TeamDeathMatch>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_TeamDeathMatch_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
