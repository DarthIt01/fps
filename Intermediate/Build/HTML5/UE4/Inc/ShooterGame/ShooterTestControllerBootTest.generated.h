// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterTestControllerBootTest_generated_h
#error "ShooterTestControllerBootTest.generated.h already included, missing '#pragma once' in ShooterTestControllerBootTest.h"
#endif
#define SHOOTERGAME_ShooterTestControllerBootTest_generated_h

#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterTestControllerBootTest(); \
	friend struct Z_Construct_UClass_UShooterTestControllerBootTest_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerBootTest, UGauntletTestControllerBootTest, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerBootTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUShooterTestControllerBootTest(); \
	friend struct Z_Construct_UClass_UShooterTestControllerBootTest_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerBootTest, UGauntletTestControllerBootTest, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerBootTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerBootTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerBootTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerBootTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerBootTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerBootTest(UShooterTestControllerBootTest&&); \
	NO_API UShooterTestControllerBootTest(const UShooterTestControllerBootTest&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerBootTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerBootTest(UShooterTestControllerBootTest&&); \
	NO_API UShooterTestControllerBootTest(const UShooterTestControllerBootTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerBootTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerBootTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerBootTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_7_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterTestControllerBootTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBootTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
