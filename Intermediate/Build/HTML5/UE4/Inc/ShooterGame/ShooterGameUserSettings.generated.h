// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterGameUserSettings_generated_h
#error "ShooterGameUserSettings.generated.h already included, missing '#pragma once' in ShooterGameUserSettings.h"
#endif
#define SHOOTERGAME_ShooterGameUserSettings_generated_h

#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterGameUserSettings(); \
	friend struct Z_Construct_UClass_UShooterGameUserSettings_Statics; \
public: \
	DECLARE_CLASS(UShooterGameUserSettings, UGameUserSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterGameUserSettings)


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUShooterGameUserSettings(); \
	friend struct Z_Construct_UClass_UShooterGameUserSettings_Statics; \
public: \
	DECLARE_CLASS(UShooterGameUserSettings, UGameUserSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterGameUserSettings)


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterGameUserSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterGameUserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterGameUserSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterGameUserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterGameUserSettings(UShooterGameUserSettings&&); \
	NO_API UShooterGameUserSettings(const UShooterGameUserSettings&); \
public:


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterGameUserSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterGameUserSettings(UShooterGameUserSettings&&); \
	NO_API UShooterGameUserSettings(const UShooterGameUserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterGameUserSettings); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterGameUserSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterGameUserSettings)


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GraphicsQuality() { return STRUCT_OFFSET(UShooterGameUserSettings, GraphicsQuality); } \
	FORCEINLINE static uint32 __PPO__bIsLanMatch() { return STRUCT_OFFSET(UShooterGameUserSettings, bIsLanMatch); } \
	FORCEINLINE static uint32 __PPO__bIsDedicatedServer() { return STRUCT_OFFSET(UShooterGameUserSettings, bIsDedicatedServer); }


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_7_PROLOG
#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_INCLASS \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h_10_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterGameUserSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterGameUserSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_ShooterGameUserSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
