// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterGame_FreeForAll_generated_h
#error "ShooterGame_FreeForAll.generated.h already included, missing '#pragma once' in ShooterGame_FreeForAll.h"
#endif
#define SHOOTERGAME_ShooterGame_FreeForAll_generated_h

#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterGame_FreeForAll(); \
	friend struct Z_Construct_UClass_AShooterGame_FreeForAll_Statics; \
public: \
	DECLARE_CLASS(AShooterGame_FreeForAll, AShooterGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGame_FreeForAll)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAShooterGame_FreeForAll(); \
	friend struct Z_Construct_UClass_AShooterGame_FreeForAll_Statics; \
public: \
	DECLARE_CLASS(AShooterGame_FreeForAll, AShooterGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGame_FreeForAll)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGame_FreeForAll(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGame_FreeForAll) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGame_FreeForAll); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGame_FreeForAll); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGame_FreeForAll(AShooterGame_FreeForAll&&); \
	NO_API AShooterGame_FreeForAll(const AShooterGame_FreeForAll&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGame_FreeForAll(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGame_FreeForAll(AShooterGame_FreeForAll&&); \
	NO_API AShooterGame_FreeForAll(const AShooterGame_FreeForAll&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGame_FreeForAll); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGame_FreeForAll); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGame_FreeForAll)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WinnerPlayerState() { return STRUCT_OFFSET(AShooterGame_FreeForAll, WinnerPlayerState); }


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_9_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterGame_FreeForAll."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterGame_FreeForAll>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Online_ShooterGame_FreeForAll_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
