// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterCheatManager_generated_h
#error "ShooterCheatManager.generated.h already included, missing '#pragma once' in ShooterCheatManager.h"
#endif
#define SHOOTERGAME_ShooterCheatManager_generated_h

#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnBot) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnBot(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCheat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Cheat(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeTeam) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_NewTeamNumber); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeTeam(Z_Param_NewTeamNumber); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execForceMatchStart) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ForceMatchStart(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleMatchTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleMatchTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleInfiniteClip) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleInfiniteClip(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleInfiniteAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleInfiniteAmmo(); \
		P_NATIVE_END; \
	}


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnBot) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SpawnBot(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execCheat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Cheat(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeTeam) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_NewTeamNumber); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeTeam(Z_Param_NewTeamNumber); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execForceMatchStart) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ForceMatchStart(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleMatchTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleMatchTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleInfiniteClip) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleInfiniteClip(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleInfiniteAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleInfiniteAmmo(); \
		P_NATIVE_END; \
	}


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterCheatManager(); \
	friend struct Z_Construct_UClass_UShooterCheatManager_Statics; \
public: \
	DECLARE_CLASS(UShooterCheatManager, UCheatManager, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterCheatManager) \
	DECLARE_WITHIN(AShooterPlayerController)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUShooterCheatManager(); \
	friend struct Z_Construct_UClass_UShooterCheatManager_Statics; \
public: \
	DECLARE_CLASS(UShooterCheatManager, UCheatManager, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterCheatManager) \
	DECLARE_WITHIN(AShooterPlayerController)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterCheatManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterCheatManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterCheatManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterCheatManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterCheatManager(UShooterCheatManager&&); \
	NO_API UShooterCheatManager(const UShooterCheatManager&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterCheatManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterCheatManager(UShooterCheatManager&&); \
	NO_API UShooterCheatManager(const UShooterCheatManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterCheatManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterCheatManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterCheatManager)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_7_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h_10_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterCheatManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterCheatManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Player_ShooterCheatManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
