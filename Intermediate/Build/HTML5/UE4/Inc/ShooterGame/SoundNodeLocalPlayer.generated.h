// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_SoundNodeLocalPlayer_generated_h
#error "SoundNodeLocalPlayer.generated.h already included, missing '#pragma once' in SoundNodeLocalPlayer.h"
#endif
#define SHOOTERGAME_SoundNodeLocalPlayer_generated_h

#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundNodeLocalPlayer(); \
	friend struct Z_Construct_UClass_USoundNodeLocalPlayer_Statics; \
public: \
	DECLARE_CLASS(USoundNodeLocalPlayer, USoundNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(USoundNodeLocalPlayer)


#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSoundNodeLocalPlayer(); \
	friend struct Z_Construct_UClass_USoundNodeLocalPlayer_Statics; \
public: \
	DECLARE_CLASS(USoundNodeLocalPlayer, USoundNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(USoundNodeLocalPlayer)


#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundNodeLocalPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundNodeLocalPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundNodeLocalPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundNodeLocalPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundNodeLocalPlayer(USoundNodeLocalPlayer&&); \
	NO_API USoundNodeLocalPlayer(const USoundNodeLocalPlayer&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundNodeLocalPlayer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundNodeLocalPlayer(USoundNodeLocalPlayer&&); \
	NO_API USoundNodeLocalPlayer(const USoundNodeLocalPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundNodeLocalPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundNodeLocalPlayer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundNodeLocalPlayer)


#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_11_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundNodeLocalPlayer."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class USoundNodeLocalPlayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Sound_SoundNodeLocalPlayer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
