// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterDamageType_generated_h
#error "ShooterDamageType.generated.h already included, missing '#pragma once' in ShooterDamageType.h"
#endif
#define SHOOTERGAME_ShooterDamageType_generated_h

#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterDamageType(); \
	friend struct Z_Construct_UClass_UShooterDamageType_Statics; \
public: \
	DECLARE_CLASS(UShooterDamageType, UDamageType, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterDamageType)


#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUShooterDamageType(); \
	friend struct Z_Construct_UClass_UShooterDamageType_Statics; \
public: \
	DECLARE_CLASS(UShooterDamageType, UDamageType, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterDamageType)


#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterDamageType(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterDamageType) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterDamageType); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterDamageType); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterDamageType(UShooterDamageType&&); \
	NO_API UShooterDamageType(const UShooterDamageType&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterDamageType(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterDamageType(UShooterDamageType&&); \
	NO_API UShooterDamageType(const UShooterDamageType&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterDamageType); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterDamageType); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterDamageType)


#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_8_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h_11_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterDamageType."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterDamageType>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Weapons_ShooterDamageType_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
