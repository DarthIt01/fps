// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterOptionsWidgetStyle_generated_h
#error "ShooterOptionsWidgetStyle.generated.h already included, missing '#pragma once' in ShooterOptionsWidgetStyle.h"
#endif
#define SHOOTERGAME_ShooterOptionsWidgetStyle_generated_h

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShooterOptionsStyle_Statics; \
	SHOOTERGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FSlateWidgetStyle Super;


template<> SHOOTERGAME_API UScriptStruct* StaticStruct<struct FShooterOptionsStyle>();

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterOptionsWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterOptionsWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterOptionsWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterOptionsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUShooterOptionsWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterOptionsWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterOptionsWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterOptionsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterOptionsWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterOptionsWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterOptionsWidgetStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(UShooterOptionsWidgetStyle&&); \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(const UShooterOptionsWidgetStyle&); \
public:


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(UShooterOptionsWidgetStyle&&); \
	SHOOTERGAME_API UShooterOptionsWidgetStyle(const UShooterOptionsWidgetStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterOptionsWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterOptionsWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterOptionsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_43_PROLOG
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_INCLASS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h_46_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterOptionsWidgetStyle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterOptionsWidgetStyle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterOptionsWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
