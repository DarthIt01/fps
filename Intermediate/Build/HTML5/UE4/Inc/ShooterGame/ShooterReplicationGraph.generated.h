// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterReplicationGraph_generated_h
#error "ShooterReplicationGraph.generated.h already included, missing '#pragma once' in ShooterReplicationGraph.h"
#endif
#define SHOOTERGAME_ShooterReplicationGraph_generated_h

#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraph(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraph, UReplicationGraph, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraph)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_INCLASS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraph(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraph_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraph, UReplicationGraph, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraph)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterReplicationGraph(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterReplicationGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraph); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraph(UShooterReplicationGraph&&); \
	NO_API UShooterReplicationGraph(const UShooterReplicationGraph&); \
public:


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraph(UShooterReplicationGraph&&); \
	NO_API UShooterReplicationGraph(const UShooterReplicationGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraph); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraph); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShooterReplicationGraph)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_31_PROLOG
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_INCLASS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_34_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterReplicationGraph>();

#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraphNode_AlwaysRelevant_ForConnection(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraphNode_AlwaysRelevant_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_INCLASS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraphNode_AlwaysRelevant_ForConnection(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraphNode_AlwaysRelevant_ForConnection_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraphNode_AlwaysRelevant_ForConnection); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection&&); \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection(const UShooterReplicationGraphNode_AlwaysRelevant_ForConnection&); \
public:


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection&&); \
	NO_API UShooterReplicationGraphNode_AlwaysRelevant_ForConnection(const UShooterReplicationGraphNode_AlwaysRelevant_ForConnection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraphNode_AlwaysRelevant_ForConnection); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LastPawn() { return STRUCT_OFFSET(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection, LastPawn); } \
	FORCEINLINE static uint32 __PPO__PastRelevantActors() { return STRUCT_OFFSET(UShooterReplicationGraphNode_AlwaysRelevant_ForConnection, PastRelevantActors); }


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_83_PROLOG
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_INCLASS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_86_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterReplicationGraphNode_AlwaysRelevant_ForConnection>();

#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraphNode_PlayerStateFrequencyLimiter(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraphNode_PlayerStateFrequencyLimiter_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_INCLASS \
private: \
	static void StaticRegisterNativesUShooterReplicationGraphNode_PlayerStateFrequencyLimiter(); \
	friend struct Z_Construct_UClass_UShooterReplicationGraphNode_PlayerStateFrequencyLimiter_Statics; \
public: \
	DECLARE_CLASS(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter, UReplicationGraphNode, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterReplicationGraphNode_PlayerStateFrequencyLimiter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraphNode_PlayerStateFrequencyLimiter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraphNode_PlayerStateFrequencyLimiter(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter&&); \
	NO_API UShooterReplicationGraphNode_PlayerStateFrequencyLimiter(const UShooterReplicationGraphNode_PlayerStateFrequencyLimiter&); \
public:


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterReplicationGraphNode_PlayerStateFrequencyLimiter(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter&&); \
	NO_API UShooterReplicationGraphNode_PlayerStateFrequencyLimiter(const UShooterReplicationGraphNode_PlayerStateFrequencyLimiter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterReplicationGraphNode_PlayerStateFrequencyLimiter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShooterReplicationGraphNode_PlayerStateFrequencyLimiter)


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_124_PROLOG
#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_INCLASS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h_127_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterReplicationGraphNode_PlayerStateFrequencyLimiter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_Online_ShooterReplicationGraph_h


#define FOREACH_ENUM_ECLASSREPNODEMAPPING(op) \
	op(EClassRepNodeMapping::NotRouted) \
	op(EClassRepNodeMapping::RelevantAllConnections) \
	op(EClassRepNodeMapping::Spatialize_Static) \
	op(EClassRepNodeMapping::Spatialize_Dynamic) \
	op(EClassRepNodeMapping::Spatialize_Dormancy) 

enum class EClassRepNodeMapping : uint32;
template<> SHOOTERGAME_API UEnum* StaticEnum<EClassRepNodeMapping>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
