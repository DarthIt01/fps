// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterCharacterMovement_generated_h
#error "ShooterCharacterMovement.generated.h already included, missing '#pragma once' in ShooterCharacterMovement.h"
#endif
#define SHOOTERGAME_ShooterCharacterMovement_generated_h

#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterCharacterMovement(); \
	friend struct Z_Construct_UClass_UShooterCharacterMovement_Statics; \
public: \
	DECLARE_CLASS(UShooterCharacterMovement, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterCharacterMovement)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUShooterCharacterMovement(); \
	friend struct Z_Construct_UClass_UShooterCharacterMovement_Statics; \
public: \
	DECLARE_CLASS(UShooterCharacterMovement, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterCharacterMovement)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterCharacterMovement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterCharacterMovement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterCharacterMovement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterCharacterMovement(UShooterCharacterMovement&&); \
	NO_API UShooterCharacterMovement(const UShooterCharacterMovement&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterCharacterMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterCharacterMovement(UShooterCharacterMovement&&); \
	NO_API UShooterCharacterMovement(const UShooterCharacterMovement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterCharacterMovement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterCharacterMovement); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterCharacterMovement)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_10_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterCharacterMovement."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterCharacterMovement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Player_ShooterCharacterMovement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
