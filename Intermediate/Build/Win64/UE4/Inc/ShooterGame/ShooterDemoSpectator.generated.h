// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterDemoSpectator_generated_h
#error "ShooterDemoSpectator.generated.h already included, missing '#pragma once' in ShooterDemoSpectator.h"
#endif
#define SHOOTERGAME_ShooterDemoSpectator_generated_h

#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterDemoSpectator(); \
	friend struct Z_Construct_UClass_AShooterDemoSpectator_Statics; \
public: \
	DECLARE_CLASS(AShooterDemoSpectator, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterDemoSpectator)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAShooterDemoSpectator(); \
	friend struct Z_Construct_UClass_AShooterDemoSpectator_Statics; \
public: \
	DECLARE_CLASS(AShooterDemoSpectator, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterDemoSpectator)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterDemoSpectator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterDemoSpectator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterDemoSpectator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterDemoSpectator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterDemoSpectator(AShooterDemoSpectator&&); \
	NO_API AShooterDemoSpectator(const AShooterDemoSpectator&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterDemoSpectator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterDemoSpectator(AShooterDemoSpectator&&); \
	NO_API AShooterDemoSpectator(const AShooterDemoSpectator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterDemoSpectator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterDemoSpectator); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterDemoSpectator)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_9_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterDemoSpectator."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterDemoSpectator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Player_ShooterDemoSpectator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
