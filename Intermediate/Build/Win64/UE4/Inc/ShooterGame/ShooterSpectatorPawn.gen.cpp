// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ShooterGame/Public/Player/ShooterSpectatorPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShooterSpectatorPawn() {}
// Cross Module References
	SHOOTERGAME_API UClass* Z_Construct_UClass_AShooterSpectatorPawn_NoRegister();
	SHOOTERGAME_API UClass* Z_Construct_UClass_AShooterSpectatorPawn();
	ENGINE_API UClass* Z_Construct_UClass_ASpectatorPawn();
	UPackage* Z_Construct_UPackage__Script_ShooterGame();
// End Cross Module References
	void AShooterSpectatorPawn::StaticRegisterNativesAShooterSpectatorPawn()
	{
	}
	UClass* Z_Construct_UClass_AShooterSpectatorPawn_NoRegister()
	{
		return AShooterSpectatorPawn::StaticClass();
	}
	struct Z_Construct_UClass_AShooterSpectatorPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AShooterSpectatorPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ASpectatorPawn,
		(UObject* (*)())Z_Construct_UPackage__Script_ShooterGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterSpectatorPawn_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Player/ShooterSpectatorPawn.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Player/ShooterSpectatorPawn.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AShooterSpectatorPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AShooterSpectatorPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AShooterSpectatorPawn_Statics::ClassParams = {
		&AShooterSpectatorPawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002A0u,
		METADATA_PARAMS(Z_Construct_UClass_AShooterSpectatorPawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AShooterSpectatorPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AShooterSpectatorPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AShooterSpectatorPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AShooterSpectatorPawn, 1591816126);
	template<> SHOOTERGAME_API UClass* StaticClass<AShooterSpectatorPawn>()
	{
		return AShooterSpectatorPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AShooterSpectatorPawn(Z_Construct_UClass_AShooterSpectatorPawn, &AShooterSpectatorPawn::StaticClass, TEXT("/Script/ShooterGame"), TEXT("AShooterSpectatorPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AShooterSpectatorPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
