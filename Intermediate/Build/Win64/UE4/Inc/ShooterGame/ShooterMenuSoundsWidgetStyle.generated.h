// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterMenuSoundsWidgetStyle_generated_h
#error "ShooterMenuSoundsWidgetStyle.generated.h already included, missing '#pragma once' in ShooterMenuSoundsWidgetStyle.h"
#endif
#define SHOOTERGAME_ShooterMenuSoundsWidgetStyle_generated_h

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShooterMenuSoundsStyle_Statics; \
	SHOOTERGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FSlateWidgetStyle Super;


template<> SHOOTERGAME_API UScriptStruct* StaticStruct<struct FShooterMenuSoundsStyle>();

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterMenuSoundsWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterMenuSoundsWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterMenuSoundsWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterMenuSoundsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUShooterMenuSoundsWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterMenuSoundsWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterMenuSoundsWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterMenuSoundsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterMenuSoundsWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterMenuSoundsWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterMenuSoundsWidgetStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(UShooterMenuSoundsWidgetStyle&&); \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(const UShooterMenuSoundsWidgetStyle&); \
public:


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(UShooterMenuSoundsWidgetStyle&&); \
	SHOOTERGAME_API UShooterMenuSoundsWidgetStyle(const UShooterMenuSoundsWidgetStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterMenuSoundsWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterMenuSoundsWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterMenuSoundsWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_43_PROLOG
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_INCLASS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h_46_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterMenuSoundsWidgetStyle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterMenuSoundsWidgetStyle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterMenuSoundsWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
