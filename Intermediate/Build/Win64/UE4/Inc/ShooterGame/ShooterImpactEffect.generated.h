// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterImpactEffect_generated_h
#error "ShooterImpactEffect.generated.h already included, missing '#pragma once' in ShooterImpactEffect.h"
#endif
#define SHOOTERGAME_ShooterImpactEffect_generated_h

#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterImpactEffect(); \
	friend struct Z_Construct_UClass_AShooterImpactEffect_Statics; \
public: \
	DECLARE_CLASS(AShooterImpactEffect, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterImpactEffect)


#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAShooterImpactEffect(); \
	friend struct Z_Construct_UClass_AShooterImpactEffect_Statics; \
public: \
	DECLARE_CLASS(AShooterImpactEffect, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterImpactEffect)


#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterImpactEffect(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterImpactEffect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterImpactEffect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterImpactEffect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterImpactEffect(AShooterImpactEffect&&); \
	NO_API AShooterImpactEffect(const AShooterImpactEffect&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterImpactEffect(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterImpactEffect(AShooterImpactEffect&&); \
	NO_API AShooterImpactEffect(const AShooterImpactEffect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterImpactEffect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterImpactEffect); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterImpactEffect)


#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_12_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterImpactEffect."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterImpactEffect>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Effects_ShooterImpactEffect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
