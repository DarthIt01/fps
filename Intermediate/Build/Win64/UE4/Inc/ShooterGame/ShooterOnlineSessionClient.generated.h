// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterOnlineSessionClient_generated_h
#error "ShooterOnlineSessionClient.generated.h already included, missing '#pragma once' in ShooterOnlineSessionClient.h"
#endif
#define SHOOTERGAME_ShooterOnlineSessionClient_generated_h

#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterOnlineSessionClient(); \
	friend struct Z_Construct_UClass_UShooterOnlineSessionClient_Statics; \
public: \
	DECLARE_CLASS(UShooterOnlineSessionClient, UOnlineSessionClient, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterOnlineSessionClient)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUShooterOnlineSessionClient(); \
	friend struct Z_Construct_UClass_UShooterOnlineSessionClient_Statics; \
public: \
	DECLARE_CLASS(UShooterOnlineSessionClient, UOnlineSessionClient, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterOnlineSessionClient)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterOnlineSessionClient(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterOnlineSessionClient) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterOnlineSessionClient); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterOnlineSessionClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterOnlineSessionClient(UShooterOnlineSessionClient&&); \
	NO_API UShooterOnlineSessionClient(const UShooterOnlineSessionClient&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterOnlineSessionClient(UShooterOnlineSessionClient&&); \
	NO_API UShooterOnlineSessionClient(const UShooterOnlineSessionClient&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterOnlineSessionClient); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterOnlineSessionClient); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShooterOnlineSessionClient)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_8_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterOnlineSessionClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Online_ShooterOnlineSessionClient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
