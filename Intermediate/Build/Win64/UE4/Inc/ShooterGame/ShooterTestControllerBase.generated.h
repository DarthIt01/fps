// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterTestControllerBase_generated_h
#error "ShooterTestControllerBase.generated.h already included, missing '#pragma once' in ShooterTestControllerBase.h"
#endif
#define SHOOTERGAME_ShooterTestControllerBase_generated_h

#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterTestControllerBase(); \
	friend struct Z_Construct_UClass_UShooterTestControllerBase_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerBase, UGauntletTestController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerBase)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUShooterTestControllerBase(); \
	friend struct Z_Construct_UClass_UShooterTestControllerBase_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerBase, UGauntletTestController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerBase)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerBase(UShooterTestControllerBase&&); \
	NO_API UShooterTestControllerBase(const UShooterTestControllerBase&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerBase(UShooterTestControllerBase&&); \
	NO_API UShooterTestControllerBase(const UShooterTestControllerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerBase)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_17_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterTestControllerBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
