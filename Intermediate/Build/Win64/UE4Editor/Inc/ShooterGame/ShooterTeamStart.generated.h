// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterTeamStart_generated_h
#error "ShooterTeamStart.generated.h already included, missing '#pragma once' in ShooterTeamStart.h"
#endif
#define SHOOTERGAME_ShooterTeamStart_generated_h

#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterTeamStart(); \
	friend struct Z_Construct_UClass_AShooterTeamStart_Statics; \
public: \
	DECLARE_CLASS(AShooterTeamStart, APlayerStart, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterTeamStart)


#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_INCLASS \
private: \
	static void StaticRegisterNativesAShooterTeamStart(); \
	friend struct Z_Construct_UClass_AShooterTeamStart_Statics; \
public: \
	DECLARE_CLASS(AShooterTeamStart, APlayerStart, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterTeamStart)


#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterTeamStart(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterTeamStart) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterTeamStart); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterTeamStart); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterTeamStart(AShooterTeamStart&&); \
	NO_API AShooterTeamStart(const AShooterTeamStart&); \
public:


#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterTeamStart(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterTeamStart(AShooterTeamStart&&); \
	NO_API AShooterTeamStart(const AShooterTeamStart&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterTeamStart); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterTeamStart); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterTeamStart)


#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_7_PROLOG
#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_INCLASS \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h_10_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterTeamStart."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterTeamStart>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_ShooterTeamStart_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
