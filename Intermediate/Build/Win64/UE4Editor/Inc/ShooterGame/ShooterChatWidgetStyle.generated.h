// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterChatWidgetStyle_generated_h
#error "ShooterChatWidgetStyle.generated.h already included, missing '#pragma once' in ShooterChatWidgetStyle.h"
#endif
#define SHOOTERGAME_ShooterChatWidgetStyle_generated_h

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShooterChatStyle_Statics; \
	SHOOTERGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FSlateWidgetStyle Super;


template<> SHOOTERGAME_API UScriptStruct* StaticStruct<struct FShooterChatStyle>();

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterChatWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterChatWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterChatWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterChatWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_INCLASS \
private: \
	static void StaticRegisterNativesUShooterChatWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterChatWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterChatWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterChatWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterChatWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterChatWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterChatWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterChatWidgetStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterChatWidgetStyle(UShooterChatWidgetStyle&&); \
	SHOOTERGAME_API UShooterChatWidgetStyle(const UShooterChatWidgetStyle&); \
public:


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterChatWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterChatWidgetStyle(UShooterChatWidgetStyle&&); \
	SHOOTERGAME_API UShooterChatWidgetStyle(const UShooterChatWidgetStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterChatWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterChatWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterChatWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_71_PROLOG
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_INCLASS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h_74_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterChatWidgetStyle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterChatWidgetStyle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterChatWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
