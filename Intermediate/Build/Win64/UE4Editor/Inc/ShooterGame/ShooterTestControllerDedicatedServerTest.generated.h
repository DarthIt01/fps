// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterTestControllerDedicatedServerTest_generated_h
#error "ShooterTestControllerDedicatedServerTest.generated.h already included, missing '#pragma once' in ShooterTestControllerDedicatedServerTest.h"
#endif
#define SHOOTERGAME_ShooterTestControllerDedicatedServerTest_generated_h

#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterTestControllerDedicatedServerTest(); \
	friend struct Z_Construct_UClass_UShooterTestControllerDedicatedServerTest_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerDedicatedServerTest, UShooterTestControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerDedicatedServerTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUShooterTestControllerDedicatedServerTest(); \
	friend struct Z_Construct_UClass_UShooterTestControllerDedicatedServerTest_Statics; \
public: \
	DECLARE_CLASS(UShooterTestControllerDedicatedServerTest, UShooterTestControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UShooterTestControllerDedicatedServerTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerDedicatedServerTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerDedicatedServerTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerDedicatedServerTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerDedicatedServerTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerDedicatedServerTest(UShooterTestControllerDedicatedServerTest&&); \
	NO_API UShooterTestControllerDedicatedServerTest(const UShooterTestControllerDedicatedServerTest&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShooterTestControllerDedicatedServerTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShooterTestControllerDedicatedServerTest(UShooterTestControllerDedicatedServerTest&&); \
	NO_API UShooterTestControllerDedicatedServerTest(const UShooterTestControllerDedicatedServerTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShooterTestControllerDedicatedServerTest); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterTestControllerDedicatedServerTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterTestControllerDedicatedServerTest)


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_8_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterTestControllerDedicatedServerTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Tests_ShooterTestControllerDedicatedServerTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
