// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_BTDecorator_HasLoSTo_generated_h
#error "BTDecorator_HasLoSTo.generated.h already included, missing '#pragma once' in BTDecorator_HasLoSTo.h"
#endif
#define SHOOTERGAME_BTDecorator_HasLoSTo_generated_h

#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTDecorator_HasLoSTo(); \
	friend struct Z_Construct_UClass_UBTDecorator_HasLoSTo_Statics; \
public: \
	DECLARE_CLASS(UBTDecorator_HasLoSTo, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UBTDecorator_HasLoSTo)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUBTDecorator_HasLoSTo(); \
	friend struct Z_Construct_UClass_UBTDecorator_HasLoSTo_Statics; \
public: \
	DECLARE_CLASS(UBTDecorator_HasLoSTo, UBTDecorator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UBTDecorator_HasLoSTo)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTDecorator_HasLoSTo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTDecorator_HasLoSTo) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTDecorator_HasLoSTo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTDecorator_HasLoSTo); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTDecorator_HasLoSTo(UBTDecorator_HasLoSTo&&); \
	NO_API UBTDecorator_HasLoSTo(const UBTDecorator_HasLoSTo&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTDecorator_HasLoSTo(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTDecorator_HasLoSTo(UBTDecorator_HasLoSTo&&); \
	NO_API UBTDecorator_HasLoSTo(const UBTDecorator_HasLoSTo&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTDecorator_HasLoSTo); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTDecorator_HasLoSTo); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTDecorator_HasLoSTo)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EnemyKey() { return STRUCT_OFFSET(UBTDecorator_HasLoSTo, EnemyKey); }


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_9_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BTDecorator_HasLoSTo."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UBTDecorator_HasLoSTo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Bots_BTDecorator_HasLoSTo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
