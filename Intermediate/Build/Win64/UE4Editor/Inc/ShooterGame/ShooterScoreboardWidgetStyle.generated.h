// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterScoreboardWidgetStyle_generated_h
#error "ShooterScoreboardWidgetStyle.generated.h already included, missing '#pragma once' in ShooterScoreboardWidgetStyle.h"
#endif
#define SHOOTERGAME_ShooterScoreboardWidgetStyle_generated_h

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShooterScoreboardStyle_Statics; \
	SHOOTERGAME_API static class UScriptStruct* StaticStruct(); \
	typedef FSlateWidgetStyle Super;


template<> SHOOTERGAME_API UScriptStruct* StaticStruct<struct FShooterScoreboardStyle>();

#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShooterScoreboardWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterScoreboardWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterScoreboardWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterScoreboardWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_INCLASS \
private: \
	static void StaticRegisterNativesUShooterScoreboardWidgetStyle(); \
	friend struct Z_Construct_UClass_UShooterScoreboardWidgetStyle_Statics; \
public: \
	DECLARE_CLASS(UShooterScoreboardWidgetStyle, USlateWidgetStyleContainerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), SHOOTERGAME_API) \
	DECLARE_SERIALIZER(UShooterScoreboardWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterScoreboardWidgetStyle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterScoreboardWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterScoreboardWidgetStyle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(UShooterScoreboardWidgetStyle&&); \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(const UShooterScoreboardWidgetStyle&); \
public:


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(UShooterScoreboardWidgetStyle&&); \
	SHOOTERGAME_API UShooterScoreboardWidgetStyle(const UShooterScoreboardWidgetStyle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SHOOTERGAME_API, UShooterScoreboardWidgetStyle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShooterScoreboardWidgetStyle); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShooterScoreboardWidgetStyle)


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_64_PROLOG
#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_INCLASS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h_67_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterScoreboardWidgetStyle."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UShooterScoreboardWidgetStyle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Private_UI_Style_ShooterScoreboardWidgetStyle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
