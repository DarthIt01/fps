// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterGameSession_generated_h
#error "ShooterGameSession.generated.h already included, missing '#pragma once' in ShooterGameSession.h"
#endif
#define SHOOTERGAME_ShooterGameSession_generated_h

#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterGameSession(); \
	friend struct Z_Construct_UClass_AShooterGameSession_Statics; \
public: \
	DECLARE_CLASS(AShooterGameSession, AGameSession, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGameSession)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_INCLASS \
private: \
	static void StaticRegisterNativesAShooterGameSession(); \
	friend struct Z_Construct_UClass_AShooterGameSession_Statics; \
public: \
	DECLARE_CLASS(AShooterGameSession, AGameSession, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterGameSession)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGameSession(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGameSession) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGameSession); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGameSession); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGameSession(AShooterGameSession&&); \
	NO_API AShooterGameSession(const AShooterGameSession&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterGameSession(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterGameSession(AShooterGameSession&&); \
	NO_API AShooterGameSession(const AShooterGameSession&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterGameSession); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterGameSession); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterGameSession)


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_31_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h_34_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterGameSession."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterGameSession>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Online_ShooterGameSession_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
