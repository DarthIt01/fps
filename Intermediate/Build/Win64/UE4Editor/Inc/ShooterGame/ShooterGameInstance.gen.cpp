// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ShooterGame/Public/ShooterGameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShooterGameInstance() {}
// Cross Module References
	SHOOTERGAME_API UEnum* Z_Construct_UEnum_ShooterGame_EOnlineMode();
	UPackage* Z_Construct_UPackage__Script_ShooterGame();
	SHOOTERGAME_API UClass* Z_Construct_UClass_UShooterGameInstance_NoRegister();
	SHOOTERGAME_API UClass* Z_Construct_UClass_UShooterGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
// End Cross Module References
	static UEnum* EOnlineMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ShooterGame_EOnlineMode, Z_Construct_UPackage__Script_ShooterGame(), TEXT("EOnlineMode"));
		}
		return Singleton;
	}
	template<> SHOOTERGAME_API UEnum* StaticEnum<EOnlineMode>()
	{
		return EOnlineMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOnlineMode(EOnlineMode_StaticEnum, TEXT("/Script/ShooterGame"), TEXT("EOnlineMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ShooterGame_EOnlineMode_Hash() { return 956711920U; }
	UEnum* Z_Construct_UEnum_ShooterGame_EOnlineMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ShooterGame();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOnlineMode"), 0, Get_Z_Construct_UEnum_ShooterGame_EOnlineMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOnlineMode::Offline", (int64)EOnlineMode::Offline },
				{ "EOnlineMode::LAN", (int64)EOnlineMode::LAN },
				{ "EOnlineMode::Online", (int64)EOnlineMode::Online },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "LAN.Name", "EOnlineMode::LAN" },
				{ "ModuleRelativePath", "Public/ShooterGameInstance.h" },
				{ "Offline.Name", "EOnlineMode::Offline" },
				{ "Online.Name", "EOnlineMode::Online" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ShooterGame,
				nullptr,
				"EOnlineMode",
				"EOnlineMode",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UShooterGameInstance::StaticRegisterNativesUShooterGameInstance()
	{
	}
	UClass* Z_Construct_UClass_UShooterGameInstance_NoRegister()
	{
		return UShooterGameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UShooterGameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MainMenuMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MainMenuMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WelcomeScreenMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WelcomeScreenMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShooterGameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_ShooterGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShooterGameInstance_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ShooterGameInstance.h" },
		{ "ModuleRelativePath", "Public/ShooterGameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_MainMenuMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/ShooterGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_MainMenuMap = { "MainMenuMap", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShooterGameInstance, MainMenuMap), METADATA_PARAMS(Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_MainMenuMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_MainMenuMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_WelcomeScreenMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/ShooterGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_WelcomeScreenMap = { "WelcomeScreenMap", nullptr, (EPropertyFlags)0x0040000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShooterGameInstance, WelcomeScreenMap), METADATA_PARAMS(Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_WelcomeScreenMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_WelcomeScreenMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UShooterGameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_MainMenuMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShooterGameInstance_Statics::NewProp_WelcomeScreenMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShooterGameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShooterGameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShooterGameInstance_Statics::ClassParams = {
		&UShooterGameInstance::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UShooterGameInstance_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UShooterGameInstance_Statics::PropPointers),
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UShooterGameInstance_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UShooterGameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShooterGameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShooterGameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShooterGameInstance, 3516391079);
	template<> SHOOTERGAME_API UClass* StaticClass<UShooterGameInstance>()
	{
		return UShooterGameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShooterGameInstance(Z_Construct_UClass_UShooterGameInstance, &UShooterGameInstance::StaticClass, TEXT("/Script/ShooterGame"), TEXT("UShooterGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShooterGameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
