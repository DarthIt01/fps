// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ShooterGame/Public/Effects/ShooterImpactEffect.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShooterImpactEffect() {}
// Cross Module References
	SHOOTERGAME_API UClass* Z_Construct_UClass_AShooterImpactEffect_NoRegister();
	SHOOTERGAME_API UClass* Z_Construct_UClass_AShooterImpactEffect();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ShooterGame();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	SHOOTERGAME_API UScriptStruct* Z_Construct_UScriptStruct_FDecalData();
	ENGINE_API UClass* Z_Construct_UClass_USoundCue_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
// End Cross Module References
	void AShooterImpactEffect::StaticRegisterNativesAShooterImpactEffect()
	{
	}
	UClass* Z_Construct_UClass_AShooterImpactEffect_NoRegister()
	{
		return AShooterImpactEffect::StaticClass();
	}
	struct Z_Construct_UClass_AShooterImpactEffect_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceHit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SurfaceHit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDecal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultDecal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FleshSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FleshSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GrassSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GrassSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlassSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GlassSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WoodSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WoodSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetalSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MetalSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirtSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DirtSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConcreteSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConcreteSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSound_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultSound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FleshFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FleshFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GrassFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GrassFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GlassFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GlassFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WoodFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WoodFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetalFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MetalFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirtFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DirtFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConcreteFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConcreteFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultFX;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AShooterImpactEffect_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ShooterGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//\n// Spawnable effect for weapon hit impact - NOT replicated to clients\n// Each impact type should be defined as separate blueprint\n//\n" },
		{ "IncludePath", "Effects/ShooterImpactEffect.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "Spawnable effect for weapon hit impact - NOT replicated to clients\nEach impact type should be defined as separate blueprint" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_SurfaceHit_MetaData[] = {
		{ "Category", "Surface" },
		{ "Comment", "/** surface data for spawning */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "surface data for spawning" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_SurfaceHit = { "SurfaceHit", nullptr, (EPropertyFlags)0x0010008000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, SurfaceHit), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_SurfaceHit_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_SurfaceHit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultDecal_MetaData[] = {
		{ "Category", "Defaults" },
		{ "Comment", "/** default decal when material specific override doesn't exist */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "default decal when material specific override doesn't exist" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultDecal = { "DefaultDecal", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, DefaultDecal), Z_Construct_UScriptStruct_FDecalData, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultDecal_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultDecal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on flesh */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on flesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshSound = { "FleshSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, FleshSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on grass */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on grass" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassSound = { "GrassSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, GrassSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on glass */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on glass" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassSound = { "GlassSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, GlassSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on wood */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on wood" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodSound = { "WoodSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, WoodSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on metal */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on metal" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalSound = { "MetalSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, MetalSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on water */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on water" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterSound = { "WaterSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, WaterSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on dirt */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on dirt" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtSound = { "DirtSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, DirtSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteSound_MetaData[] = {
		{ "Category", "Sound" },
		{ "Comment", "/** impact FX on concrete */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on concrete" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteSound = { "ConcreteSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, ConcreteSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultSound_MetaData[] = {
		{ "Category", "Defaults" },
		{ "Comment", "/** default impact sound used when material specific override doesn't exist */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "default impact sound used when material specific override doesn't exist" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultSound = { "DefaultSound", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, DefaultSound), Z_Construct_UClass_USoundCue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultSound_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultSound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on flesh */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on flesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshFX = { "FleshFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, FleshFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on grass */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on grass" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassFX = { "GrassFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, GrassFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on glass */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on glass" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassFX = { "GlassFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, GlassFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on wood */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on wood" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodFX = { "WoodFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, WoodFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on metal */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on metal" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalFX = { "MetalFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, MetalFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on water */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on water" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterFX = { "WaterFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, WaterFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on dirt */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on dirt" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtFX = { "DirtFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, DirtFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteFX_MetaData[] = {
		{ "Category", "Visual" },
		{ "Comment", "/** impact FX on concrete */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "impact FX on concrete" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteFX = { "ConcreteFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, ConcreteFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultFX_MetaData[] = {
		{ "Category", "Defaults" },
		{ "Comment", "/** default impact FX used when material specific override doesn't exist */" },
		{ "ModuleRelativePath", "Public/Effects/ShooterImpactEffect.h" },
		{ "ToolTip", "default impact FX used when material specific override doesn't exist" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultFX = { "DefaultFX", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShooterImpactEffect, DefaultFX), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultFX_MetaData, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultFX_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AShooterImpactEffect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_SurfaceHit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultDecal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultSound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_FleshFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GrassFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_GlassFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WoodFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_MetalFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_WaterFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DirtFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_ConcreteFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShooterImpactEffect_Statics::NewProp_DefaultFX,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AShooterImpactEffect_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AShooterImpactEffect>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AShooterImpactEffect_Statics::ClassParams = {
		&AShooterImpactEffect::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AShooterImpactEffect_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::PropPointers),
		0,
		0x008000A1u,
		METADATA_PARAMS(Z_Construct_UClass_AShooterImpactEffect_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AShooterImpactEffect_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AShooterImpactEffect()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AShooterImpactEffect_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AShooterImpactEffect, 3800730127);
	template<> SHOOTERGAME_API UClass* StaticClass<AShooterImpactEffect>()
	{
		return AShooterImpactEffect::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AShooterImpactEffect(Z_Construct_UClass_AShooterImpactEffect, &AShooterImpactEffect::StaticClass, TEXT("/Script/ShooterGame"), TEXT("AShooterImpactEffect"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AShooterImpactEffect);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
