// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_BTTask_FindPointNearEnemy_generated_h
#error "BTTask_FindPointNearEnemy.generated.h already included, missing '#pragma once' in BTTask_FindPointNearEnemy.h"
#endif
#define SHOOTERGAME_BTTask_FindPointNearEnemy_generated_h

#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_FindPointNearEnemy(); \
	friend struct Z_Construct_UClass_UBTTask_FindPointNearEnemy_Statics; \
public: \
	DECLARE_CLASS(UBTTask_FindPointNearEnemy, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindPointNearEnemy)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_FindPointNearEnemy(); \
	friend struct Z_Construct_UClass_UBTTask_FindPointNearEnemy_Statics; \
public: \
	DECLARE_CLASS(UBTTask_FindPointNearEnemy, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindPointNearEnemy)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindPointNearEnemy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindPointNearEnemy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindPointNearEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindPointNearEnemy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindPointNearEnemy(UBTTask_FindPointNearEnemy&&); \
	NO_API UBTTask_FindPointNearEnemy(const UBTTask_FindPointNearEnemy&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindPointNearEnemy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindPointNearEnemy(UBTTask_FindPointNearEnemy&&); \
	NO_API UBTTask_FindPointNearEnemy(const UBTTask_FindPointNearEnemy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindPointNearEnemy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindPointNearEnemy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindPointNearEnemy)


#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_9_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BTTask_FindPointNearEnemy."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class UBTTask_FindPointNearEnemy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Bots_BTTask_FindPointNearEnemy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
