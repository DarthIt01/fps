// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FKey;
struct FVector;
struct FRotator;
#ifdef SHOOTERGAME_ShooterPlayerController_generated_h
#error "ShooterPlayerController.generated.h already included, missing '#pragma once' in ShooterPlayerController.h"
#endif
#define SHOOTERGAME_ShooterPlayerController_generated_h

#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_RPC_WRAPPERS \
	virtual bool ServerSuicide_Validate(); \
	virtual void ServerSuicide_Implementation(); \
	virtual bool ServerSay_Validate(const FString& ); \
	virtual void ServerSay_Implementation(const FString& Msg); \
	virtual bool ServerCheat_Validate(const FString& ); \
	virtual void ServerCheat_Implementation(const FString& Msg); \
	virtual void ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds); \
	virtual void ClientEndOnlineGame_Implementation(); \
	virtual void ClientStartOnlineGame_Implementation(); \
	virtual void ClientGameStarted_Implementation(); \
	virtual void ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation); \
 \
	DECLARE_FUNCTION(execServerSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerSuicide_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSuicide_Validate")); \
			return; \
		} \
		P_THIS->ServerSuicide_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Suicide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnLeaderboardReadComplete) \
	{ \
		P_GET_UBOOL(Z_Param_bWasSuccessful); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnLeaderboardReadComplete(Z_Param_bWasSuccessful); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetGodMode) \
	{ \
		P_GET_UBOOL(Z_Param_bEnable); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetGodMode(Z_Param_bEnable); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSay) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerSay_Validate(Z_Param_Msg)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSay_Validate")); \
			return; \
		} \
		P_THIS->ServerSay_Implementation(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSay) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Say(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerCheat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerCheat_Validate(Z_Param_Msg)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerCheat_Validate")); \
			return; \
		} \
		P_THIS->ServerCheat_Implementation(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSimulateInputKey) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_bPressed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SimulateInputKey(Z_Param_Key,Z_Param_bPressed); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientSendRoundEndEvent) \
	{ \
		P_GET_UBOOL(Z_Param_bIsWinner); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ExpendedTimeInSeconds); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientSendRoundEndEvent_Implementation(Z_Param_bIsWinner,Z_Param_ExpendedTimeInSeconds); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientEndOnlineGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientEndOnlineGame_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientStartOnlineGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientStartOnlineGame_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientGameStarted) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientGameStarted_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientSetSpectatorCamera) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_CameraLocation); \
		P_GET_STRUCT(FRotator,Z_Param_CameraRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientSetSpectatorCamera_Implementation(Z_Param_CameraLocation,Z_Param_CameraRotation); \
		P_NATIVE_END; \
	}


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool ServerSuicide_Validate(); \
	virtual void ServerSuicide_Implementation(); \
	virtual bool ServerSay_Validate(const FString& ); \
	virtual void ServerSay_Implementation(const FString& Msg); \
	virtual bool ServerCheat_Validate(const FString& ); \
	virtual void ServerCheat_Implementation(const FString& Msg); \
	virtual void ClientSendRoundEndEvent_Implementation(bool bIsWinner, int32 ExpendedTimeInSeconds); \
	virtual void ClientEndOnlineGame_Implementation(); \
	virtual void ClientStartOnlineGame_Implementation(); \
	virtual void ClientGameStarted_Implementation(); \
	virtual void ClientSetSpectatorCamera_Implementation(FVector CameraLocation, FRotator CameraRotation); \
 \
	DECLARE_FUNCTION(execServerSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerSuicide_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSuicide_Validate")); \
			return; \
		} \
		P_THIS->ServerSuicide_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Suicide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnLeaderboardReadComplete) \
	{ \
		P_GET_UBOOL(Z_Param_bWasSuccessful); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnLeaderboardReadComplete(Z_Param_bWasSuccessful); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetGodMode) \
	{ \
		P_GET_UBOOL(Z_Param_bEnable); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetGodMode(Z_Param_bEnable); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSay) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerSay_Validate(Z_Param_Msg)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSay_Validate")); \
			return; \
		} \
		P_THIS->ServerSay_Implementation(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSay) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Say(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerCheat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Msg); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!P_THIS->ServerCheat_Validate(Z_Param_Msg)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerCheat_Validate")); \
			return; \
		} \
		P_THIS->ServerCheat_Implementation(Z_Param_Msg); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSimulateInputKey) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_bPressed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SimulateInputKey(Z_Param_Key,Z_Param_bPressed); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientSendRoundEndEvent) \
	{ \
		P_GET_UBOOL(Z_Param_bIsWinner); \
		P_GET_PROPERTY(UIntProperty,Z_Param_ExpendedTimeInSeconds); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientSendRoundEndEvent_Implementation(Z_Param_bIsWinner,Z_Param_ExpendedTimeInSeconds); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientEndOnlineGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientEndOnlineGame_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientStartOnlineGame) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientStartOnlineGame_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientGameStarted) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientGameStarted_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientSetSpectatorCamera) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_CameraLocation); \
		P_GET_STRUCT(FRotator,Z_Param_CameraRotation); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ClientSetSpectatorCamera_Implementation(Z_Param_CameraLocation,Z_Param_CameraRotation); \
		P_NATIVE_END; \
	}


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_EVENT_PARMS \
	struct ShooterPlayerController_eventClientSendRoundEndEvent_Parms \
	{ \
		bool bIsWinner; \
		int32 ExpendedTimeInSeconds; \
	}; \
	struct ShooterPlayerController_eventClientSetSpectatorCamera_Parms \
	{ \
		FVector CameraLocation; \
		FRotator CameraRotation; \
	}; \
	struct ShooterPlayerController_eventServerCheat_Parms \
	{ \
		FString Msg; \
	}; \
	struct ShooterPlayerController_eventServerSay_Parms \
	{ \
		FString Msg; \
	};


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_CALLBACK_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterPlayerController(); \
	friend struct Z_Construct_UClass_AShooterPlayerController_Statics; \
public: \
	DECLARE_CLASS(AShooterPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterPlayerController) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAShooterPlayerController(); \
	friend struct Z_Construct_UClass_AShooterPlayerController_Statics; \
public: \
	DECLARE_CLASS(AShooterPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterPlayerController) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterPlayerController(AShooterPlayerController&&); \
	NO_API AShooterPlayerController(const AShooterPlayerController&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterPlayerController(AShooterPlayerController&&); \
	NO_API AShooterPlayerController(const AShooterPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterPlayerController)


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_11_PROLOG \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_EVENT_PARMS


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_CALLBACK_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_CALLBACK_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterPlayerController."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Player_ShooterPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
