// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOOTERGAME_ShooterBot_generated_h
#error "ShooterBot.generated.h already included, missing '#pragma once' in ShooterBot.h"
#endif
#define SHOOTERGAME_ShooterBot_generated_h

#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_RPC_WRAPPERS
#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShooterBot(); \
	friend struct Z_Construct_UClass_AShooterBot_Statics; \
public: \
	DECLARE_CLASS(AShooterBot, AShooterCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterBot)


#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAShooterBot(); \
	friend struct Z_Construct_UClass_AShooterBot_Statics; \
public: \
	DECLARE_CLASS(AShooterBot, AShooterCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ShooterGame"), NO_API) \
	DECLARE_SERIALIZER(AShooterBot)


#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterBot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterBot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterBot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterBot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterBot(AShooterBot&&); \
	NO_API AShooterBot(const AShooterBot&); \
public:


#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShooterBot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShooterBot(AShooterBot&&); \
	NO_API AShooterBot(const AShooterBot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShooterBot); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShooterBot); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShooterBot)


#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_PRIVATE_PROPERTY_OFFSET
#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_8_PROLOG
#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_RPC_WRAPPERS \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_INCLASS \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_PRIVATE_PROPERTY_OFFSET \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_INCLASS_NO_PURE_DECLS \
	ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h_11_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ShooterBot."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTERGAME_API UClass* StaticClass<class AShooterBot>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ShooterGame_Source_ShooterGame_Public_Bots_ShooterBot_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
